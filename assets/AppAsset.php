<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'web/css/site.css',
        'web/css/style.css',
        'web/js/extjs/resources/css/ext-all.css',
        'web/js/extjs/resources/css/ext-all-gray.css',
        'web/css/bootstrap-datepicker.min.css'
    ];
    public $js = [
        'web/js/extjs/ext-all.js',
        'web/js/extjs/locale/ext-lang-ru.js',
        'web/js/bootstrap-datepicker.min.js',
        'web/js/bootstrap-datepicker.ru.min.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
