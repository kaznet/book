<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

use yii\web\UploadedFile;


use app\models\ContactForm;

use app\models\Book;

class BookController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['catalog', 'edit', 'ajax'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['catalog', 'edit', 'ajax'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }



    public function actionCatalog()
    {
        $bookModel = new Book();

        $authorModel = new \app\models\Authors();
        $data['authors'] = $authorModel->get_records()['data'];

        $data['books'] = $bookModel->get_records();
        return $this->render("catalog", $data);
    }


    public function actionEdit()
    {
        $id = ArrayHelper::getValue($_GET, 'id');
        $data['errors'] = [];



        $bookModel = new Book();

        if(isset($_POST['save']))
        {
            $name = ArrayHelper::getValue($_POST, 'name');
            $author_id = ArrayHelper::getValue($_POST, 'author_id');
            $date = ArrayHelper::getValue($_POST, 'date');


            $bookModel = $bookModel->findOne($id);
            $bookModel->name = $name;
            $bookModel->author_id = $author_id;
            $bookModel->date = $date;
            $bookModel->date_update = date("Y-m-d");

            if($bookModel->validate() AND !empty($_FILES['preview']['tmp_name']))
            {
                if(is_uploaded_file($_FILES['preview']['tmp_name']))
                {
                    $allowed_extensions = array('jpg','png');
                    $ext = explode('.',$_FILES['preview']['name']);
                    $ext = strtolower(end($ext));

                    $fname =  $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.\yii\helpers\Url::base().'web\covers\cover'.$id.'.jpg';

                    if( in_array($ext, $allowed_extensions) ) {
                        move_uploaded_file($_FILES['preview']['tmp_name'], $fname);

                        $bookModel->preview = basename($fname);
                    }
                }

            }
            
            if($bookModel->save() === FALSE)
            {
                $data['errors'] = $bookModel->getErrors();
            }
            else
            {
                $this->redirect(\yii\helpers\Url::toRoute(['book/catalog']));
            }
        }

        $data['book'] = $bookModel->get_record($id);
        if($data['book'] == null)
        {
            return ;
        }


        $authorModel = new \app\models\Authors();
        $data['authors'] = $authorModel->get_records()['data'];

        return $this->render('edit', $data);
    }


    public function actionAjax()
    {
        $action = ArrayHelper::getValue($_GET, 'action');

        $bookModel = new Book();

        switch ($action){
            case "get_books":
                $filter['filter'] = ArrayHelper::getValue($_GET, 'filter', []);
                $filter['start'] = ArrayHelper::getValue($_GET, 'start', 0);
                $filter['limit'] = ArrayHelper::getValue($_GET, 'start', 50);
                $filter['sort'] = ArrayHelper::getValue($_GET, 'start', 50);

                $data = $bookModel->get_records($filter);
                echo json_encode($data);
                break;

            case "book_info":
                $data = [];
                $id = ArrayHelper::getValue($_POST, 'book_id', 1);
                $data = $bookModel->get_record($id);
                if($data == NULL) break;

                $authorModel = new \app\models\Authors();
                $author = $authorModel->get_record($data->author_id);

                echo \Yii::$app->view->renderFile('@app/views/book/bookView.php', ['book' => $data, 'author' => $author]);
                break;

            case "delete":
                $data = ['success' => true, 'message' => ''];
                $id = ArrayHelper::getValue($_POST, 'book_id', 1);
                $data['success'] = $bookModel->delete_record($id);
                echo json_encode($data);
                break;
        }
    }


}
