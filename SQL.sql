-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.38-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных yii2test
CREATE DATABASE IF NOT EXISTS `yii2test` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `yii2test`;


-- Дамп структуры для таблица yii2test.authors
CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы yii2test.authors: 2 rows
DELETE FROM `authors`;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `date_create`) VALUES
	(1, 'Tom', 'Watson', '2016-03-13 09:49:48'),
	(2, 'Nelly', 'Gibson', '2016-03-13 14:22:55');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;


-- Дамп структуры для таблица yii2test.books
CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `preview` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы yii2test.books: 12 rows
DELETE FROM `books`;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` (`id`, `name`, `preview`, `author_id`, `date`, `date_create`, `date_update`) VALUES
	(1, 'Well 1', 'cover1.jpg', 1, '2016-03-11', '2016-03-13 08:47:06', '2016-03-14 00:00:00'),
	(4, 'Book2 7777', 'cover2.jpg', 1, '2016-03-10', '2016-03-13 08:47:06', '0000-00-00 00:00:00'),
	(5, 'Pulse', 'cover3.jpg', 1, '2016-03-12', '2016-03-13 08:47:06', '2016-03-14 00:00:00'),
	(6, 'Book14 ', 'cover1.jpg', 1, '2016-03-12', '2016-03-13 08:47:06', '0000-00-00 00:00:00'),
	(7, 'Book5', 'cover2.jpg', 1, '2016-03-12', '2016-03-13 08:47:06', '0000-00-00 00:00:00'),
	(8, 'Book6 ', 'cover3.jpg', 1, '2016-03-12', '2016-03-13 08:47:06', '0000-00-00 00:00:00'),
	(9, '7', 'cover1.jpg', 2, '2016-03-12', '2016-03-13 08:47:06', '0000-00-00 00:00:00'),
	(10, 'Book8 ', 'cover1.jpg', 1, '2016-03-12', '2016-03-13 08:47:06', '0000-00-00 00:00:00'),
	(11, 'Book9', 'cover1.jpg', 2, '2016-03-12', '2016-03-13 08:47:06', '0000-00-00 00:00:00'),
	(12, 'Book1 0', 'cover12.jpg', 1, '2016-03-12', '2016-03-13 08:47:06', '2016-03-14 00:00:00'),
	(13, 'Book1 1', 'cover13.jpg', 1, '2016-03-12', '2016-03-13 08:47:06', '2016-03-14 00:00:00'),
	(14, 'Book1 234234234', '', 1, '2016-03-12', '2016-03-13 20:31:56', NULL);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
