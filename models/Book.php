<?php



namespace app\models;

use yii\web\UploadedFile;

class Book extends \app\models\Database
{
    public static function tableName(){
        return 'books';
    }

    public function rules()
    {
        return [

            [['name', 'author_id'], 'required'],

            ['date', 'date', 'format'=>'yyyy-mm-dd'],

        ];
    }


    /**
     * @return  array
     */
    protected function selecting_fields()
    {
        return ['books.*', "CONCAT_WS(' ',first_name, last_name) author_name"];
    }

    protected function extra_methods(&$query)
    {
        $query->join('LEFT JOIN', 'authors a', 'a.id = books.author_id');
    }
}