<?php



namespace app\models;

use yii\base\Model;
use app\models\Database;

class Authors extends \app\models\Database
{
    /*public static function tableName(){
        return 'authors';
    }*/

    /**
     * @return  array
     */
    protected function selecting_fields()
    {
        return ['authors.*', "CONCAT_WS(' ', first_name, last_name) author_name"];
    }

}