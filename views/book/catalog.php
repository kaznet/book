<?php
$this->title = 'Каталог книг';

use \yii\helpers\Url;
?>

<div id="filterFormHolder"></div>

<div id="booksGridHolder"></div>

<div id="bookInfoModal" class="modal fade" tabindex="-1"  role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="previewModal" class="modal fade" tabindex="-1"  role="dialog" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="min-width: 50px!important;">
            <img src="" style="max-width: 500px" />
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script lang="text/javascript">
    
    Book = {
        show_book: function (id) {
            var modal = $("#bookInfoModal");
            modal.find(".modal-content").html('');
            modal.modal('show');

            $.ajax({
                url: '<?php echo Url::toRoute(['book/ajax', 'action' => 'book_info']); ?>',
                method: 'POST',
                data: {book_id: id},
                dataType: 'html',
                success: function (data) {
                    modal.find(".modal-content").html(data);
                }
            });
        },

        delete_book: function (id, callback) {
            Ext.getCmp('booksGrid').setLoading();
            $.ajax({
                url: '<?php echo Url::toRoute(['book/ajax', 'action' => 'delete']); ?>',
                method: 'POST',
                data: {book_id: id},
                dataType: 'json',
                success: function (data) {
                    Ext.getCmp('booksGrid').setLoading(false);
                    if(typeof callback == 'function') callback(data);
                }
            });
        },

        edit: function (id) {
            location.href = '<?php echo Url::toRoute(['book/edit']) ?>&id='+id;
        },
        
        show_cover: function (src) {
            if(!src) return false;
            var w = $("#previewModal");

            w.find('img').attr('src', src);
            w.modal({width: 1000}).modal('show');
        },

        setCookie: function(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        },

        getCookie: function(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
            }
            return "";
        },

        set_filters: function () {
            var g = Ext.getCmp('booksGrid'),
                f = Ext.getCmp('searchForm').getForm(),
                values = f.getValues(),
                s = g.getStore();

            s.getProxy().extraParems = {};

            for(var k in values)
            {
                s.getProxy().extraParams[k] = values[k];
            }

            Grid.set_filter(g, 'name', values.name);
            Grid.set_filter(g, 'author_id', values.author_id);

            if(!values.author_id) values.author_id = '';

            Book.setCookie('name', values.name);
            Book.setCookie('author_id', values.author_id);
            Book.setCookie('start_date', values.start_date);
            Book.setCookie('end_date', values.end_date);

            if(values.start_date) values.start_date = new Date(values.start_date);
            if(values.end_date) values.end_date = new Date(values.end_date);

            var ad = {after: values.start_date, before: values.end_date};
            Grid.set_filter(g, 'date', ad, 'date');
        }
    };

    Grid = {
        set_filter: function (grid, field, value, type) {
            if(!type) type = 'string';
            //var value = aFilter.comparison == 'gt' ? {after: dateValue} : {before: dateValue};
            if(type == 'date' && typeof value == 'string')
            {
                value = { on: new Date(value) };
            }

            var gridFilter = grid.filters.getFilter(field);

            if (!gridFilter) {
                gridFilter = grid.filters.addFilter({
                    active: true,
                    type: type,
                    dataIndex: field,
                });

                gridFilter.menu.show();
                gridFilter.setValue(value);
                gridFilter.menu.hide();
            } else {
                gridFilter.setActive(true);
            }

            Ext.Function.defer(function(){
                gridFilter = grid.filters.getFilter(field);
                gridFilter.setValue(value);
            }, 10);
        }
    }

    Ext.Loader.setConfig({enabled: true});
    Ext.Loader.setPath('Ext.ux', '<?php echo Url::base() ?>/web/js/extjs/ux/');
    Ext.require(['Ext.data.*','Ext.tip.*', 'Ext.grid.*','Ext.form.*','Ext.ux.grid.FiltersFeature']);


    Ext.onReady(function () {

        var store = Ext.create('Ext.data.JsonStore', {
            fields: [
                'id','name','preview','date_update','date','author_id','author_name', {name: 'date_create', type: 'date',  dateFormat: 'd.m.Y', defaultValue: undefined}
            ],
            autoLoad: false,
            proxy: {
                type: 'ajax',
                url: '<?php echo Url::toRoute(['book/ajax', 'action' => 'get_books']) ?>',
                reader: {
                    type: 'json',
                    root: 'data',
                    idProperty: 'id',
                    totalProperty: 'total'
                }
            }
        });


        Ext.create('Ext.grid.Panel', {
            store: store,
            id: 'booksGrid',
            features: [{
                ftype: 'filters'
            }],
            columns: [
                { text: 'ID', width: 90, dataIndex: 'id' },
                { text: 'Название', flex: 1, dataIndex: 'name' },
                {
                    text: 'Превью', dataIndex: 'preview', width: 60,
                    renderer: function (v,s,r)
                    {
                        if(!v) return '';
                        var src = '<?php echo Url::base() ?>/web/covers/'+v;

                        var h = '<a href="#" onclick="Book.show_cover(\''+src+'\');return false"><img src="'+src+'" class="book-cover" /></a>';

                        return h;
                    }

                },
                { text: 'Автор', flex: 1, dataIndex: 'author_name' },
                { text: 'Дата выхода книги', width: 110, dataIndex: 'date' },
                { text: 'Дата добавления', flex: 1, dataIndex: 'date_create' },

                { text: 'author_id', flex: 1, dataIndex: 'author_id', hidden: true },

                {
                    menuDisabled: true,
                    sortable: false,
                    text: 'Действия',
                    xtype: 'actioncolumn',
                    align: 'right',
                    width: 120,
                    items: [
                        {
                            iconCls: 'sprite sprite-draw',
                            tooltip: 'Редактировать',
                            handler: function (a,b,c,d,e,rec) {
                                Book.edit(rec.data.id);
                            }
                        },
                        {
                            iconCls: 'sprite sprite-medical',
                            tooltip: 'Просмотр',
                            handler: function (a,b,c,d,e,rec) {
                                Book.show_book(rec.data.id);
                            }
                        },
                        {
                            iconCls: ' sprite sprite-delete',
                            tooltip: 'Удалить',
                            handler: function (g,b,c,d,e,rec) {
                                Ext.MessageBox.confirm(this.up('grid').title, 'Запись будет удален! Продолжить?', function (v) {
                                    if(v != 'yes') return false;
                                    Book.delete_book(rec.data.id, function (data) {
                                        if(data.success == false){
                                            Ext.MessageBox.alert(g.title, 'Не удалось удалить!');
                                            return;
                                        }
                                        g.getStore().remove(rec);
                                    });
                                });
                            }
                        }
                    ]
                }
            ],
            height: 350,
            width: '100%',
            title: 'Каталог книг',
            renderTo: 'booksGridHolder',
            viewConfig: {
                stripeRows: true,
                enableTextSelection: true
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: store,
                displayInfo: true,
                displayMsg: 'Displaying topics {0} - {1} of {2}',
                emptyMsg: "No topics to display"
            })
        });

        Ext.create('Ext.form.Panel', {
            margin: '0 0 10 0',
            id: 'searchForm',
            defaults: { margin: '10 10 10 10'},
            items: [
                {
                    layout: 'hbox',
                    border: false,
                    items: [
                        {
                            xtype: 'combo',
                            fieldLabel: 'Автор',
                            name: 'author_id',
                            labelWidth: 60,
                            valueField: 'id',
                            displayField: 'author_name',
                            value: Book.getCookie('author_id'),
                            store: Ext.create('Ext.data.Store', {
                                fields: ['id', 'author_name'],
                                data: <?php echo json_encode($authors) ?>
                            }),
                            value: Book.getCookie('author_id')


                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Название',
                            name: 'name',
                            labelWidth: 70,
                            width: 450,
                            margin: '0 0 0 15',
                            value: Book.getCookie('name')
                        }
                    ]
                },
                {
                    layout: 'hbox',
                    border: false,
                    defaults: {format: 'd.m.Y', submitFormat: 'Y-m-d'},
                    items: [
                        {
                            xtype: 'datefield',
                            fieldLabel: "Дата выхода книги",
                            name: 'start_date',
                            labelWidth: 150,
                            value: Book.getCookie('start_date')
                        },
                        {
                            xtype: 'datefield',
                            name: 'end_date',
                            margin: '0 0 0 15',
                            value: Book.getCookie('end_date')
                        },

                        {
                            xtype: 'button',
                            text: 'Искать',
                            align: 'right',
                            margin: '0 0 10 100',
                            handler: function () {
                                Book.set_filters();
                            }
                        }

                    ]
                }
            ],
            renderTo: 'filterFormHolder'
        });


        Book.set_filters();
    });
</script>