<?php
use \yii\helpers\Html;

?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><?php echo $book->name ?></h4>
</div>
<div class="modal-body">
    <p style="text-align: center">
    <?php if(!empty($book->preview)) : ?>
        <?php echo Html::img('@web/web/covers/'.$book->preview, ['alt' => $book->name,'class' => 'cover']) ?>
    <?php endif ?>
    </p>

    <p>
        <label>Автор:</label>
        <?php echo isset($author->author_name) ? $author->author_name : ""; ?>
    </p>
</div>


<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
</div>