<?php
use yii\helpers\Html;

$this->title = 'Редактирование книги'  . $book->name;

?>

<form role="form" action=""  method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-sm-9">
            <div class="form-group">
                <label for="name">Название:</label>
                <input class="form-control" id="name" name="name" value="<?php echo $book->name ?>">
                <?php if(!empty($errors['name'])) : ?>
                    <?php foreach ($errors['name'] AS $error) : ?>
                        <p class="alert alert-danger"><?php echo $error ?></p>
                    <?php endforeach ?>
                <?php endif ?>
            </div>

            <div class="form-group">
                <label for="author">Автор:</label>
                <select class="form-control" id="author" name="author_id" value="<?php echo $book->date ?>">
                    <?php foreach ($authors AS $author) : ?>
                        <option value="<?php echo $author['id'] ?>"><?php echo $author['author_name'] ?></option>
                    <?php endforeach; ?>
                </select>
                <?php if(!empty($errors['author_id'])) : ?>
                    <?php foreach ($errors['author_id'] AS $error) : ?>
                        <p class="alert alert-danger"><?php echo $error ?></p>
                    <?php endforeach ?>
                <?php endif ?>
            </div>


            <div class="input-group" style="margin-bottom: 20px">
                <label for="preview">Превью:</label>
                <input class="form-control" id="preview" type="file" name="preview">

                <?php if(!empty($errors['preview'])) : ?>
                    <?php foreach ($errors['preview'] AS $error) : ?>
                        <p class="alert alert-danger"><?php echo $error ?></p>
                    <?php endforeach ?>
                <?php endif ?>
            </div>



            <div class="input-group date">
                <label for="date">Дата публикации:</label>
                <input class="form-control" id="date" name="date" value="<?php echo $book->date ?>">

                <?php if(!empty($errors['date'])) : ?>
                    <?php foreach ($errors['date'] AS $error) : ?>
                        <p class="alert alert-danger"><?php echo $error ?></p>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
        </div>
        <div class="col-sm-3">
            <?php if(!empty($book->preview)) : ?>
                <?php echo Html::img('@web/web/covers/'.$book->preview, ['alt' => $book->name,'class' => 'cover', 'style' => 'max-width: 190px']) ?>
            <?php endif ?>
        </div>


    </div>

    </br>
    <button type="submit" name="save" class="btn btn-default">Сохранить</button>
</form>
<script>
    $(document).ready(function () {
        $('#date').datepicker({
            maxViewMode: 1,
            todayBtn: "linked",
            format: {
                toDisplay: function (date, format, language) {
                    var d = new Date(date);
                    d.setDate(d.getDate() - 7);
                    var dt = d.toISOString();
                    return dt.replace(/T.+$/, '');
                },
                toValue: function (date, format, language) {
                    var d = new Date(date);
                    d.setDate(d.getDate() + 7);
                    return new Date(d);
                }
            },
        });
    })

</script>